package main

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"strconv"
)

type ResponseJSON struct {
	Groups  ServerGroup `json:"groups"`
	Regions []Region    `json:"regions"`
}

type ServerGroup struct {
	VPN   []Service `json:"vpn"`
	Proxy []Service `json:"proxy"`
}

type Service struct {
	Name string `json:"name"`
	Port []int  `json:"ports"`
}

type Region struct {
	ID         string       `json:"id"`
	Name       string       `json:"name"`
	Country    string       `json:"country"`
	AutoRegion bool         `json:"auto_region"`
	Servers    []ServerList `json:"servers"`
}

type ServerList struct {
	VPN   []Server `json:"vpn"`
	Proxy []Server `json:"proxy"`
}

type Server struct {
	IP string `json:"ip"`
	CN string `json:"cn"`
}

func randomServers(vpn, proxy int) ServerList {
	serverList := ServerList{}
	for i := 0; i < vpn; i++ {
		ip := strconv.Itoa(rand.Intn(254)) + "." +
			strconv.Itoa(rand.Intn(254)) + "." +
			strconv.Itoa(rand.Intn(254)) + "." +
			strconv.Itoa(rand.Intn(254))
		name := "vpn" + strconv.Itoa(rand.Intn(99999)) + ".jennifer"
		serverList.VPN = append(serverList.VPN, Server{ip, name})
	}
	for i := 0; i < proxy; i++ {
		ip := strconv.Itoa(rand.Intn(254)) + "." +
			strconv.Itoa(rand.Intn(254)) + "." +
			strconv.Itoa(rand.Intn(254)) + "." +
			strconv.Itoa(rand.Intn(254))
		name := "proxy" + strconv.Itoa(rand.Intn(99999)) + ".jennifer"
		serverList.Proxy = append(serverList.Proxy, Server{ip, name})
	}
	return serverList
}

func main() {
	groups := ServerGroup{
		VPN: []Service{
			Service{"openvpn_tcp", []int{80, 443, 8080}},
			Service{"openvpn_udp", []int{53, 443, 8443}},
			Service{"ikev2", []int{500, 4500}},
			Service{"wireguard", []int{1337}},
			Service{"latency", []int{8888}},
		},
		Proxy: []Service{
			Service{"socks", []int{1080}},
			Service{"shadowsocks", []int{443}},
			Service{"wireguard", []int{1337}},
			Service{"latency", []int{8888}},
		},
	}

	regions := []Region{
		Region{
			"eu_ro", "Romania", "RO", true, []ServerList{randomServers(10, 2)},
		},
		Region{
			"eu_de", "Germany", "DE", false, []ServerList{randomServers(10, 2)},
		},
		Region{
			"asia_in", "India", "IN", true, []ServerList{randomServers(10, 2)},
		},
	}

	responseObject := ResponseJSON{groups, regions}

	responseJSON, err := json.Marshal(responseObject)
	if err != nil {
		fmt.Println("Could not parse data:", err)
		return
	}

	fmt.Println(string(responseJSON))
}
